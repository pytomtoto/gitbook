```
xshell
选一台linux服务器
切换到root用户
sudo -i

mkdir -p /opt/squid-server
cd /opt/squid-server
apt-get install lrzsz -y # yum install lrzsz
rz 
然后上传服务端部署压缩包至以上路径
apt-get install unzip  -y 
解压 unzip squid-server.zip
-rw-r--r-- 1 root root      248 Jun 14  2021 config.ini  服务端配置文件
-rw-r--r-- 1 root root 10235904 Jun 14  2021 squid-agent  agent端 用于分发至其他机器
-rw-r--r-- 1 root root 21242824 Jun 14  2021 squid-server 服务端程序
-rw-r--r-- 1 root root 16545774 Jun 14 10:39 squid-server.zip 压缩包
drwxr-xr-x 6 root root     4096 Jun 10 13:27 statics 静态资源目录
drwxr-xr-x 4 root root     4096 Jun 10 13:26 views 模板文件
然后
chmod +x squid-server
./squid-server >log 2>&1 &
exit
ps -ef|grep squid-server
kill pid
然后访问 主机：端口号 （端口号可以在config.ini中修改）

部署视频见微信

目前发现问题
管理IP：192.126.112.194-198
业务IP：23.252.164.1-126
业务IP：23.252.164.129-254" 
这台机器ssh连接有问题 时好时坏 对这样的机器使用部署时需多次重试
```
